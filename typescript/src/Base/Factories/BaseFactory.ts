export abstract class BaseFactory<TKey, TValue> {

	protected items: Array<{ key: TKey, value: () => TValue }> = [];

	public getItem(key: TKey): TValue {
		const item = this.items.find(item => item.key === key);
		return item ? item.value() : null;
	}
}