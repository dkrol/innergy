"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseFactory = void 0;
var BaseFactory = /** @class */ (function () {
    function BaseFactory() {
        this.items = [];
    }
    BaseFactory.prototype.getItem = function (key) {
        var item = this.items.find(function (item) { return item.key === key; });
        return item ? item.value() : null;
    };
    return BaseFactory;
}());
exports.BaseFactory = BaseFactory;
