import { ServicesDomain } from "../Domains/Services/ServicesDomain";
import { YearServicesDomain } from "../Domains/YearServices/YearServicesDomain";
import { BaseDiscountPriceDomain } from "../Domains/DiscountPrice/Base/BaseDiscountPriceDomain";
import { DiscountPriceDomainFactory } from "../Domains/DiscountPrice/Factories/DiscountPriceDomainFactory";

export class PriceCalculatorManager {
	private _servicesDomain: ServicesDomain;
	private _yearServicesDomain: YearServicesDomain
	private _discountPriceDomain: BaseDiscountPriceDomain;

	private constructor(servicesDomain: ServicesDomain, yearServicesDomain: YearServicesDomain) {
		this._servicesDomain = servicesDomain;
		this._yearServicesDomain = yearServicesDomain;
		this.initDiscountPriceDomain();
	}

	public static create(servicesDomain: ServicesDomain, yearServicesDomain: YearServicesDomain): PriceCalculatorManager {
		return new PriceCalculatorManager(servicesDomain, yearServicesDomain);
	}

	public calculateBasePrice(): number {
		return this._servicesDomain.calculatePrice(this._yearServicesDomain);
	}

	public calculateDiscountPrice(): number {
		return this._discountPriceDomain.calculateDiscount();
	}

	public calculateFinalPrice(): number {
		return this.calculateBasePrice() - this.calculateDiscountPrice();
	}

	public calculateBasePriceWithFinalPrice(): { basePrice: number, finalPrice: number } {
		return {
			basePrice: this.calculateBasePrice(),
			finalPrice: this.calculateFinalPrice()
		};
	}

	private initDiscountPriceDomain(): void {
		this._discountPriceDomain = DiscountPriceDomainFactory.create()
			.getDiscountPriceDomain(this._servicesDomain, this._yearServicesDomain);
	}
}