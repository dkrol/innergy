"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PriceCalculatorManager = void 0;
var DiscountPriceDomainFactory_1 = require("../Domains/DiscountPrice/Factories/DiscountPriceDomainFactory");
var PriceCalculatorManager = /** @class */ (function () {
    function PriceCalculatorManager(servicesDomain, yearServicesDomain) {
        this._servicesDomain = servicesDomain;
        this._yearServicesDomain = yearServicesDomain;
        this.initDiscountPriceDomain();
    }
    PriceCalculatorManager.create = function (servicesDomain, yearServicesDomain) {
        return new PriceCalculatorManager(servicesDomain, yearServicesDomain);
    };
    PriceCalculatorManager.prototype.calculateBasePrice = function () {
        return this._servicesDomain.calculatePrice(this._yearServicesDomain);
    };
    PriceCalculatorManager.prototype.calculateDiscountPrice = function () {
        return this._discountPriceDomain.calculateDiscount();
    };
    PriceCalculatorManager.prototype.calculateFinalPrice = function () {
        return this.calculateBasePrice() - this.calculateDiscountPrice();
    };
    PriceCalculatorManager.prototype.calculateBasePriceWithFinalPrice = function () {
        return {
            basePrice: this.calculateBasePrice(),
            finalPrice: this.calculateFinalPrice()
        };
    };
    PriceCalculatorManager.prototype.initDiscountPriceDomain = function () {
        this._discountPriceDomain = DiscountPriceDomainFactory_1.DiscountPriceDomainFactory.create()
            .getDiscountPriceDomain(this._servicesDomain, this._yearServicesDomain);
    };
    return PriceCalculatorManager;
}());
exports.PriceCalculatorManager = PriceCalculatorManager;
