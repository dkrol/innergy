import { ActionType, ServiceType, ServiceYear } from "./Models/Types";
import { ServiceActionHandlersFactory } from "./Domains/ServiceActions/Factories/ServiceActionHandlersFactory";
import { ServicesDomain } from "./Domains/Services/ServicesDomain";
import { YearsServicesDomain } from "./Domains/YearsServices/YearsServicesDomain";
import { PriceCalculatorManager } from "./Managers/PriceCalculatorManager";

export const updateSelectedServices = (
	previouslySelectedServices: ServiceType[],
	action: { type: ActionType; service: ServiceType }
) => {
	try {
		const handler = ServiceActionHandlersFactory.create().getItem(action.type);
		return handler.updateServices(action.service, ServicesDomain.create(previouslySelectedServices));
	} catch (e) {
		console.log(e);
	}
	return [];
};

export const calculatePrice = (
	selectedServices: ServiceType[],
	selectedYear: ServiceYear
) => {
	try {
		const services = ServicesDomain.create(selectedServices);
		const yearServices = YearsServicesDomain.create().getYearServices(selectedYear);
		const priceCalculatorManager = PriceCalculatorManager.create(services, yearServices);
		return priceCalculatorManager.calculateBasePriceWithFinalPrice();
	} catch (e) {
		console.log(e);
	}
	return { basePrice: 0, finalPrice: 0 };
};
