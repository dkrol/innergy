import { BaseDiscountPriceDecorator } from "../Base/BaseDiscountPriceDecorator";

export class WeddingSessionWithVideoRecordingDiscountPriceDecorator extends BaseDiscountPriceDecorator {
	public calculateDiscount(): number {
		const weddingSessionPrice = this.yearServices.getServicePrice("WeddingSession");
		const discount = weddingSessionPrice - 300;
		const prevDiscount = super.calculateDiscount();
		
		return prevDiscount + discount; 
	}
}