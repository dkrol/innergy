"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.PhotographyWithVideoRecordingDiscountPriceDecorator = void 0;
var BaseDiscountPriceDecorator_1 = require("../Base/BaseDiscountPriceDecorator");
var photographyWithVideoRecordingPrices = {
    2020: 2200,
    2021: 2300,
    2022: 2500,
};
var PhotographyWithVideoRecordingDiscountPriceDecorator = /** @class */ (function (_super) {
    __extends(PhotographyWithVideoRecordingDiscountPriceDecorator, _super);
    function PhotographyWithVideoRecordingDiscountPriceDecorator() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    PhotographyWithVideoRecordingDiscountPriceDecorator.prototype.calculateDiscount = function () {
        var basePrice = photographyWithVideoRecordingPrices[this.yearServices.year];
        var photographyPrice = this.yearServices.getServicePrice("Photography");
        var videoRecordingPrice = this.yearServices.getServicePrice("VideoRecording");
        var discount = photographyPrice + videoRecordingPrice - basePrice;
        var prevDiscount = _super.prototype.calculateDiscount.call(this);
        return prevDiscount + discount;
    };
    return PhotographyWithVideoRecordingDiscountPriceDecorator;
}(BaseDiscountPriceDecorator_1.BaseDiscountPriceDecorator));
exports.PhotographyWithVideoRecordingDiscountPriceDecorator = PhotographyWithVideoRecordingDiscountPriceDecorator;
