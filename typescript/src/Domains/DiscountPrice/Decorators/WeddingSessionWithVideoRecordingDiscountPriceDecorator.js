"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.WeddingSessionWithVideoRecordingDiscountPriceDecorator = void 0;
var BaseDiscountPriceDecorator_1 = require("../Base/BaseDiscountPriceDecorator");
var WeddingSessionWithVideoRecordingDiscountPriceDecorator = /** @class */ (function (_super) {
    __extends(WeddingSessionWithVideoRecordingDiscountPriceDecorator, _super);
    function WeddingSessionWithVideoRecordingDiscountPriceDecorator() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    WeddingSessionWithVideoRecordingDiscountPriceDecorator.prototype.calculateDiscount = function () {
        var weddingSessionPrice = this.yearServices.getServicePrice("WeddingSession");
        var discount = weddingSessionPrice - 300;
        var prevDiscount = _super.prototype.calculateDiscount.call(this);
        return prevDiscount + discount;
    };
    return WeddingSessionWithVideoRecordingDiscountPriceDecorator;
}(BaseDiscountPriceDecorator_1.BaseDiscountPriceDecorator));
exports.WeddingSessionWithVideoRecordingDiscountPriceDecorator = WeddingSessionWithVideoRecordingDiscountPriceDecorator;
