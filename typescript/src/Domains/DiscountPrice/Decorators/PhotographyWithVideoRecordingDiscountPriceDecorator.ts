import { BaseDiscountPriceDecorator } from "../Base/BaseDiscountPriceDecorator";

const photographyWithVideoRecordingPrices = {
	2020: 2200,
	2021: 2300,
	2022: 2500,
};

export class PhotographyWithVideoRecordingDiscountPriceDecorator extends BaseDiscountPriceDecorator {
	public calculateDiscount(): number {
		const basePrice = photographyWithVideoRecordingPrices[this.yearServices.year];
		const photographyPrice = this.yearServices.getServicePrice("Photography");
		const videoRecordingPrice = this.yearServices.getServicePrice("VideoRecording");

		const discount = photographyPrice + videoRecordingPrice - basePrice;
		const prevDiscount = super.calculateDiscount();

		return prevDiscount + discount;
	}
}