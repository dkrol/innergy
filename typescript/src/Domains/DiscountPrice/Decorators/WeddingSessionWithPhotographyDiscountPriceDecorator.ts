import { BaseDiscountPriceDecorator } from "../Base/BaseDiscountPriceDecorator";

export class WeddingSessionWithPhotographyDiscountPriceDecorator extends BaseDiscountPriceDecorator {
	public calculateDiscount(): number {
		const weddingSessionPrice = this.yearServices.getServicePrice("WeddingSession");
		const discount = this.yearServices.year === 2022 ? weddingSessionPrice : weddingSessionPrice - 300;
		const prevDiscount = super.calculateDiscount()
		
		return prevDiscount + discount;
	}
}