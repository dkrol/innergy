"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.DiscountPriceDomain = void 0;
var BaseDiscountPriceDomain_1 = require("./Base/BaseDiscountPriceDomain");
var DiscountPriceDomain = /** @class */ (function (_super) {
    __extends(DiscountPriceDomain, _super);
    function DiscountPriceDomain(yearServices) {
        return _super.call(this, yearServices) || this;
    }
    DiscountPriceDomain.create = function (yearServices) {
        return new DiscountPriceDomain(yearServices);
    };
    DiscountPriceDomain.prototype.calculateDiscount = function () {
        return 0;
    };
    return DiscountPriceDomain;
}(BaseDiscountPriceDomain_1.BaseDiscountPriceDomain));
exports.DiscountPriceDomain = DiscountPriceDomain;
