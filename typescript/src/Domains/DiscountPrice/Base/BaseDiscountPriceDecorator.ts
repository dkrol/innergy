import { BaseDiscountPriceDomain } from "./BaseDiscountPriceDomain";

export abstract class BaseDiscountPriceDecorator extends BaseDiscountPriceDomain {
	protected discountPriceDomain: BaseDiscountPriceDomain;

	constructor(discountPriceDomain: BaseDiscountPriceDomain) {
		super(discountPriceDomain.yearServices);
		this.discountPriceDomain = discountPriceDomain;
	}

	public calculateDiscount(): number {
		return this.discountPriceDomain.calculateDiscount();
	}
}