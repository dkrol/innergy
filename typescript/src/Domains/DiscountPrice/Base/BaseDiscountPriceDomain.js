"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseDiscountPriceDomain = void 0;
var BaseDiscountPriceDomain = /** @class */ (function () {
    function BaseDiscountPriceDomain(yearServices) {
        this._yearServices = yearServices;
    }
    Object.defineProperty(BaseDiscountPriceDomain.prototype, "yearServices", {
        get: function () { return this._yearServices; },
        enumerable: false,
        configurable: true
    });
    return BaseDiscountPriceDomain;
}());
exports.BaseDiscountPriceDomain = BaseDiscountPriceDomain;
