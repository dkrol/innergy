import { YearServicesDomain } from "../../YearServices/YearServicesDomain";

export abstract class BaseDiscountPriceDomain {
	private _yearServices: YearServicesDomain;

	public get yearServices(): YearServicesDomain { return this._yearServices; }

	constructor(yearServices: YearServicesDomain) {
		this._yearServices = yearServices;
	}

	abstract calculateDiscount(): number;
}