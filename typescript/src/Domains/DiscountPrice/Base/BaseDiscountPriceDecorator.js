"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseDiscountPriceDecorator = void 0;
var BaseDiscountPriceDomain_1 = require("./BaseDiscountPriceDomain");
var BaseDiscountPriceDecorator = /** @class */ (function (_super) {
    __extends(BaseDiscountPriceDecorator, _super);
    function BaseDiscountPriceDecorator(discountPriceDomain) {
        var _this = _super.call(this, discountPriceDomain.yearServices) || this;
        _this.discountPriceDomain = discountPriceDomain;
        return _this;
    }
    BaseDiscountPriceDecorator.prototype.calculateDiscount = function () {
        return this.discountPriceDomain.calculateDiscount();
    };
    return BaseDiscountPriceDecorator;
}(BaseDiscountPriceDomain_1.BaseDiscountPriceDomain));
exports.BaseDiscountPriceDecorator = BaseDiscountPriceDecorator;
