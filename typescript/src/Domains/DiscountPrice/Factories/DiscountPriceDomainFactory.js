"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DiscountPriceDomainFactory = void 0;
var DiscountPriceDomain_1 = require("../DiscountPriceDomain");
var PhotographyWithVideoRecordingDiscountPriceDecorator_1 = require("../Decorators/PhotographyWithVideoRecordingDiscountPriceDecorator");
var WeddingSessionWithPhotographyDiscountPriceDecorator_1 = require("../Decorators/WeddingSessionWithPhotographyDiscountPriceDecorator");
var WeddingSessionWithVideoRecordingDiscountPriceDecorator_1 = require("../Decorators/WeddingSessionWithVideoRecordingDiscountPriceDecorator");
var DiscountPriceDomainFactory = /** @class */ (function () {
    function DiscountPriceDomainFactory() {
    }
    DiscountPriceDomainFactory.create = function () {
        return new DiscountPriceDomainFactory();
    };
    DiscountPriceDomainFactory.prototype.getDiscountPriceDomain = function (servicesDomain, yearServicesDomain) {
        var discountPriceDomain = DiscountPriceDomain_1.DiscountPriceDomain.create(yearServicesDomain);
        if (servicesDomain.contains("Photography") && servicesDomain.contains("VideoRecording")) {
            discountPriceDomain = new PhotographyWithVideoRecordingDiscountPriceDecorator_1.PhotographyWithVideoRecordingDiscountPriceDecorator(discountPriceDomain);
        }
        if (servicesDomain.contains("WeddingSession") && servicesDomain.contains("Photography")) {
            discountPriceDomain = new WeddingSessionWithPhotographyDiscountPriceDecorator_1.WeddingSessionWithPhotographyDiscountPriceDecorator(discountPriceDomain);
        }
        else if (servicesDomain.contains("WeddingSession") && servicesDomain.contains("VideoRecording")) {
            discountPriceDomain = new WeddingSessionWithVideoRecordingDiscountPriceDecorator_1.WeddingSessionWithVideoRecordingDiscountPriceDecorator(discountPriceDomain);
        }
        return discountPriceDomain;
    };
    return DiscountPriceDomainFactory;
}());
exports.DiscountPriceDomainFactory = DiscountPriceDomainFactory;
