import { ServicesDomain } from "../../Services/ServicesDomain";
import { YearServicesDomain } from "../../YearServices/YearServicesDomain";
import { BaseDiscountPriceDomain } from "../Base/BaseDiscountPriceDomain";
import { DiscountPriceDomain } from "../DiscountPriceDomain";
import { PhotographyWithVideoRecordingDiscountPriceDecorator } from "../Decorators/PhotographyWithVideoRecordingDiscountPriceDecorator";
import { WeddingSessionWithPhotographyDiscountPriceDecorator } from "../Decorators/WeddingSessionWithPhotographyDiscountPriceDecorator";
import { WeddingSessionWithVideoRecordingDiscountPriceDecorator } from "../Decorators/WeddingSessionWithVideoRecordingDiscountPriceDecorator";

export class DiscountPriceDomainFactory {
	private constructor() { }

	public static create(): DiscountPriceDomainFactory {
		return new DiscountPriceDomainFactory();
	}

	public getDiscountPriceDomain(servicesDomain: ServicesDomain, yearServicesDomain: YearServicesDomain)
		: BaseDiscountPriceDomain {

		let discountPriceDomain: BaseDiscountPriceDomain = DiscountPriceDomain.create(yearServicesDomain);

		if (servicesDomain.contains("Photography") && servicesDomain.contains("VideoRecording")) {
			discountPriceDomain = new PhotographyWithVideoRecordingDiscountPriceDecorator(discountPriceDomain);
		}

		if (servicesDomain.contains("WeddingSession") && servicesDomain.contains("Photography")) {
			discountPriceDomain = new WeddingSessionWithPhotographyDiscountPriceDecorator(discountPriceDomain);
		}
		else if (servicesDomain.contains("WeddingSession") && servicesDomain.contains("VideoRecording")) {
			discountPriceDomain = new WeddingSessionWithVideoRecordingDiscountPriceDecorator(discountPriceDomain);
		}

		return discountPriceDomain;
	}
}