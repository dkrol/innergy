import { BaseDiscountPriceDomain } from "./Base/BaseDiscountPriceDomain";
import { YearServicesDomain } from "../YearServices/YearServicesDomain";

export class DiscountPriceDomain extends BaseDiscountPriceDomain {
	private constructor(yearServices: YearServicesDomain) {
		super(yearServices);
	}

	public static create(yearServices: YearServicesDomain): DiscountPriceDomain {
		return new DiscountPriceDomain(yearServices);
	}

	public calculateDiscount(): number {
		return 0;
	}
}