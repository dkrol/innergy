import { ServiceType } from "../../../Models/Types";
import { ServicesDomain } from "../../Services/ServicesDomain";

export interface IServiceActionHandler {
	updateServices(currentService: ServiceType, previouslySelectedServices: ServicesDomain): ServiceType[];
}