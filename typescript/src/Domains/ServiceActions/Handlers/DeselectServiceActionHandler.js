"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DeselectServiceActionHandler = void 0;
var DeselectServiceActionHandler = /** @class */ (function () {
    function DeselectServiceActionHandler() {
    }
    DeselectServiceActionHandler.prototype.updateServices = function (currentService, previouslySelectedServices) {
        if (previouslySelectedServices.contains(currentService)) {
            if (currentService === "Photography" && !previouslySelectedServices.contains("VideoRecording")) {
                previouslySelectedServices.removeService("TwoDayEvent");
            }
            previouslySelectedServices.removeService(currentService);
        }
        return previouslySelectedServices.getServices();
    };
    return DeselectServiceActionHandler;
}());
exports.DeselectServiceActionHandler = DeselectServiceActionHandler;
