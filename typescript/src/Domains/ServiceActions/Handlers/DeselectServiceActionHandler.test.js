"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var DeselectServiceActionHandler_1 = require("./DeselectServiceActionHandler");
var ServicesDomain_1 = require("../../Services/ServicesDomain");
describe("DeselectServiceActionHandler.updateServices", function () {
    test("should deselect", function () {
        var deselectServiceActionHandler = new DeselectServiceActionHandler_1.DeselectServiceActionHandler();
        var servicesDomain = ServicesDomain_1.ServicesDomain.create(["WeddingSession", "Photography"]);
        var result = deselectServiceActionHandler.updateServices("Photography", servicesDomain);
        expect(result).toEqual(["WeddingSession"]);
    });
    test("should do nothing when service not selected", function () {
        var deselectServiceActionHandler = new DeselectServiceActionHandler_1.DeselectServiceActionHandler();
        var servicesDomain = ServicesDomain_1.ServicesDomain.create(["WeddingSession", "Photography"]);
        var result = deselectServiceActionHandler.updateServices("TwoDayEvent", servicesDomain);
        expect(result).toEqual(["WeddingSession", "Photography"]);
    });
    test("should deselect related when last main service deselected", function () {
        var deselectServiceActionHandler = new DeselectServiceActionHandler_1.DeselectServiceActionHandler();
        var servicesDomain = ServicesDomain_1.ServicesDomain.create(["WeddingSession", "Photography", "TwoDayEvent"]);
        var result = deselectServiceActionHandler.updateServices("Photography", servicesDomain);
        expect(result).toEqual(["WeddingSession"]);
    });
    test("should not deselect related when at least one main service stays selected", function () {
        var deselectServiceActionHandler = new DeselectServiceActionHandler_1.DeselectServiceActionHandler();
        var servicesDomain = ServicesDomain_1.ServicesDomain.create(["WeddingSession", "Photography", "VideoRecording", "TwoDayEvent"]);
        var result = deselectServiceActionHandler.updateServices("Photography", servicesDomain);
        expect(result).toEqual(["WeddingSession", "VideoRecording", "TwoDayEvent"]);
    });
});
