import { DeselectServiceActionHandler } from "./DeselectServiceActionHandler";
import { ServicesDomain } from "../../Services/ServicesDomain";

describe("DeselectServiceActionHandler.updateServices", () => {
	test("should deselect", () => {
		const deselectServiceActionHandler = new DeselectServiceActionHandler();
		const servicesDomain = ServicesDomain.create(["WeddingSession", "Photography"]);
		const result = deselectServiceActionHandler.updateServices("Photography", servicesDomain);
		expect(result).toEqual(["WeddingSession"]);
	});

	test("should do nothing when service not selected", () => {
		const deselectServiceActionHandler = new DeselectServiceActionHandler();
		const servicesDomain = ServicesDomain.create(["WeddingSession", "Photography"]);
		const result = deselectServiceActionHandler.updateServices("TwoDayEvent", servicesDomain);
		expect(result).toEqual(["WeddingSession", "Photography"]);
	});

	test("should deselect related when last main service deselected", () => {
		const deselectServiceActionHandler = new DeselectServiceActionHandler();
		const servicesDomain = ServicesDomain.create(["WeddingSession", "Photography", "TwoDayEvent"]);
		const result = deselectServiceActionHandler.updateServices("Photography", servicesDomain);
		expect(result).toEqual(["WeddingSession"]);
	});

	test("should not deselect related when at least one main service stays selected", () => {
		const deselectServiceActionHandler = new DeselectServiceActionHandler();
		const servicesDomain = ServicesDomain.create(["WeddingSession", "Photography", "VideoRecording", "TwoDayEvent"]);
		const result = deselectServiceActionHandler.updateServices("Photography", servicesDomain);
		expect(result).toEqual(["WeddingSession", "VideoRecording", "TwoDayEvent"]);
	});
});