"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var SelectServiceActionHandler_1 = require("./SelectServiceActionHandler");
var ServicesDomain_1 = require("../../Services/ServicesDomain");
describe("SelectServiceActionHandler.updateServices", function () {
    test("should select when not selected", function () {
        var selectServiceActionHandler = new SelectServiceActionHandler_1.SelectServiceActionHandler();
        var servicesDomain = ServicesDomain_1.ServicesDomain.create([]);
        var result = selectServiceActionHandler.updateServices("Photography", servicesDomain);
        expect(result).toEqual(["Photography"]);
    });
    test("should not select the same service twice", function () {
        var selectServiceActionHandler = new SelectServiceActionHandler_1.SelectServiceActionHandler();
        var servicesDomain = ServicesDomain_1.ServicesDomain.create(["Photography"]);
        var result = selectServiceActionHandler.updateServices("Photography", servicesDomain);
        expect(result).toEqual(["Photography"]);
    });
    test("should not select related service when main service is not selected", function () {
        var selectServiceActionHandler = new SelectServiceActionHandler_1.SelectServiceActionHandler();
        var servicesDomain = ServicesDomain_1.ServicesDomain.create(["WeddingSession"]);
        var result = selectServiceActionHandler.updateServices("BlurayPackage", servicesDomain);
        expect(result).toEqual(["WeddingSession"]);
    });
    test("should select related service when main service is selected", function () {
        var selectServiceActionHandler = new SelectServiceActionHandler_1.SelectServiceActionHandler();
        var servicesDomain = ServicesDomain_1.ServicesDomain.create(["WeddingSession", "VideoRecording"]);
        var result = selectServiceActionHandler.updateServices("BlurayPackage", servicesDomain);
        expect(result).toEqual(["WeddingSession", "VideoRecording", "BlurayPackage"]);
    });
    test("should select related service when one of main services is selected", function () {
        var selectServiceActionHandler = new SelectServiceActionHandler_1.SelectServiceActionHandler();
        var servicesDomain = ServicesDomain_1.ServicesDomain.create(["WeddingSession", "Photography"]);
        var result = selectServiceActionHandler.updateServices("TwoDayEvent", servicesDomain);
        expect(result).toEqual(["WeddingSession", "Photography", "TwoDayEvent"]);
    });
});
