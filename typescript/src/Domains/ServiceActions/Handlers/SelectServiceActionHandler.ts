import { IServiceActionHandler } from "../Base/IServiceActionHandler";
import { ServiceType } from "../../../Models/Types";
import { ServicesDomain } from "../../Services/ServicesDomain";

export class SelectServiceActionHandler implements IServiceActionHandler {
	updateServices(currentService: ServiceType, previouslySelectedServices: ServicesDomain): ServiceType[] {
		if (previouslySelectedServices.isEmpty()) {
			return [currentService];
		}

		if (currentService === "BlurayPackage"
			&& previouslySelectedServices.contains("VideoRecording")) {
			previouslySelectedServices.addService("BlurayPackage");
		}

		if (currentService === "TwoDayEvent"
			&& previouslySelectedServices.contains("Photography")
			&& previouslySelectedServices.contains("WeddingSession")) {
			previouslySelectedServices.addService("TwoDayEvent");
		}

		return previouslySelectedServices.getServices();
	}
}