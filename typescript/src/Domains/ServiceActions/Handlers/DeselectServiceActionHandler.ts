import { IServiceActionHandler } from "../Base/IServiceActionHandler";
import { ServiceType } from "../../../Models/Types";
import { ServicesDomain } from "../../Services/ServicesDomain";

export class DeselectServiceActionHandler implements IServiceActionHandler {
	updateServices(currentService: ServiceType, previouslySelectedServices: ServicesDomain): ServiceType[] {
		if (previouslySelectedServices.contains(currentService)) {

			if (currentService === "Photography" && !previouslySelectedServices.contains("VideoRecording")) {
				previouslySelectedServices.removeService("TwoDayEvent");
			}

			previouslySelectedServices.removeService(currentService);
		}

		return previouslySelectedServices.getServices();
	}
}
