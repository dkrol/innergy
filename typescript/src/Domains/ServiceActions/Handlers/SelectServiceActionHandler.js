"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SelectServiceActionHandler = void 0;
var SelectServiceActionHandler = /** @class */ (function () {
    function SelectServiceActionHandler() {
    }
    SelectServiceActionHandler.prototype.updateServices = function (currentService, previouslySelectedServices) {
        if (previouslySelectedServices.isEmpty()) {
            return [currentService];
        }
        if (currentService === "BlurayPackage"
            && previouslySelectedServices.contains("VideoRecording")) {
            previouslySelectedServices.addService("BlurayPackage");
        }
        if (currentService === "TwoDayEvent"
            && previouslySelectedServices.contains("Photography")
            && previouslySelectedServices.contains("WeddingSession")) {
            previouslySelectedServices.addService("TwoDayEvent");
        }
        return previouslySelectedServices.getServices();
    };
    return SelectServiceActionHandler;
}());
exports.SelectServiceActionHandler = SelectServiceActionHandler;
