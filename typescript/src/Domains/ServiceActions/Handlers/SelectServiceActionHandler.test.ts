import { SelectServiceActionHandler } from "./SelectServiceActionHandler";
import { ServicesDomain } from "../../Services/ServicesDomain";

describe("SelectServiceActionHandler.updateServices", () => {
	test("should select when not selected", () => {
		const selectServiceActionHandler = new SelectServiceActionHandler();
		const servicesDomain = ServicesDomain.create([]);
		const result = selectServiceActionHandler.updateServices("Photography", servicesDomain);
		expect(result).toEqual(["Photography"]);
	});

	test("should not select the same service twice", () => {
		const selectServiceActionHandler = new SelectServiceActionHandler();
		const servicesDomain = ServicesDomain.create(["Photography"]);
		const result = selectServiceActionHandler.updateServices("Photography", servicesDomain);
		expect(result).toEqual(["Photography"]);
	});

	test("should not select related service when main service is not selected", () => {
		const selectServiceActionHandler = new SelectServiceActionHandler();
		const servicesDomain = ServicesDomain.create(["WeddingSession"]);
		const result = selectServiceActionHandler.updateServices("BlurayPackage", servicesDomain);
		expect(result).toEqual(["WeddingSession"]);
	});

	test("should select related service when main service is selected", () => {
		const selectServiceActionHandler = new SelectServiceActionHandler();
		const servicesDomain = ServicesDomain.create(["WeddingSession", "VideoRecording"]);
		const result = selectServiceActionHandler.updateServices("BlurayPackage", servicesDomain);
		expect(result).toEqual(["WeddingSession", "VideoRecording", "BlurayPackage"]);
	});

	test("should select related service when one of main services is selected", () => {
		const selectServiceActionHandler = new SelectServiceActionHandler();
		const servicesDomain = ServicesDomain.create(["WeddingSession", "Photography"]);
		const result = selectServiceActionHandler.updateServices("TwoDayEvent", servicesDomain);
		expect(result).toEqual(["WeddingSession", "Photography", "TwoDayEvent"]);
	});
});