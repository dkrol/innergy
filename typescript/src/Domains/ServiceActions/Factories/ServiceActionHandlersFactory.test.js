"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ServiceActionHandlersFactory_1 = require("./ServiceActionHandlersFactory");
describe.each([
    "Select",
    "Deselect"
])("ServiceActionHandlersFactory.getItem", function (actionType) {
    test("should return handler for action type", function () {
        var serviceActionHandlersFactory = ServiceActionHandlersFactory_1.ServiceActionHandlersFactory.create();
        var result = serviceActionHandlersFactory.getItem(actionType);
        expect(result).not.toEqual(undefined);
        expect(result).not.toEqual(null);
    });
});
