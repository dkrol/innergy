import { BaseFactory } from "../../../Base/Factories/BaseFactory";
import { ActionType } from "../../../Models/Types";
import { IServiceActionHandler } from "../Base/IServiceActionHandler";
import { SelectServiceActionHandler } from "../Handlers/SelectServiceActionHandler";
import { DeselectServiceActionHandler } from "../Handlers/DeselectServiceActionHandler";

export class ServiceActionHandlersFactory extends BaseFactory<ActionType, IServiceActionHandler> {
	private constructor() {
		super();
		this.items = [
		 { key: "Select", value: () => new SelectServiceActionHandler() },
		 { key: "Deselect", value: () => new DeselectServiceActionHandler() },
		];
	}

	static create(): ServiceActionHandlersFactory {
		return new ServiceActionHandlersFactory();
	}
}