"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.ServiceActionHandlersFactory = void 0;
var BaseFactory_1 = require("../../../Base/Factories/BaseFactory");
var SelectServiceActionHandler_1 = require("../Handlers/SelectServiceActionHandler");
var DeselectServiceActionHandler_1 = require("../Handlers/DeselectServiceActionHandler");
var ServiceActionHandlersFactory = /** @class */ (function (_super) {
    __extends(ServiceActionHandlersFactory, _super);
    function ServiceActionHandlersFactory() {
        var _this = _super.call(this) || this;
        _this.items = [
            { key: "Select", value: function () { return new SelectServiceActionHandler_1.SelectServiceActionHandler(); } },
            { key: "Deselect", value: function () { return new DeselectServiceActionHandler_1.DeselectServiceActionHandler(); } },
        ];
        return _this;
    }
    ServiceActionHandlersFactory.create = function () {
        return new ServiceActionHandlersFactory();
    };
    return ServiceActionHandlersFactory;
}(BaseFactory_1.BaseFactory));
exports.ServiceActionHandlersFactory = ServiceActionHandlersFactory;
