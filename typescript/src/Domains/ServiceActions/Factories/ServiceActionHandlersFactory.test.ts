import { ActionType } from "../../../Models/Types";
import { ServiceActionHandlersFactory } from "./ServiceActionHandlersFactory";

describe.each([
	"Select", 
	"Deselect"
])("ServiceActionHandlersFactory.getItem", (actionType: ActionType) => {
	test("should return handler for action type", () => {
		const serviceActionHandlersFactory = ServiceActionHandlersFactory.create();
		const result = serviceActionHandlersFactory.getItem(actionType);
		expect(result).not.toEqual(undefined);
		expect(result).not.toEqual(null);
	});
});