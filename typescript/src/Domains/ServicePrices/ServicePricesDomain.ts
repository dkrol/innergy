import { ServiceType } from "../../Models/Types";

export class ServicePricesDomain {
	private _type: ServiceType;
	private _price: number;

	public get type(): ServiceType { return this._type; }
	public get price(): number { return this._price; }

	private constructor(type: ServiceType, price: number) {
		this._type = type;
		this._price = price >= 0 ? price : 0;
	}

	public static create(type: ServiceType, price: number): ServicePricesDomain {
		return new ServicePricesDomain(type, price);
	}
}