"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ServicePricesDomain = void 0;
var ServicePricesDomain = /** @class */ (function () {
    function ServicePricesDomain(type, price) {
        this._type = type;
        this._price = price >= 0 ? price : 0;
    }
    Object.defineProperty(ServicePricesDomain.prototype, "type", {
        get: function () { return this._type; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ServicePricesDomain.prototype, "price", {
        get: function () { return this._price; },
        enumerable: false,
        configurable: true
    });
    ServicePricesDomain.create = function (type, price) {
        return new ServicePricesDomain(type, price);
    };
    return ServicePricesDomain;
}());
exports.ServicePricesDomain = ServicePricesDomain;
