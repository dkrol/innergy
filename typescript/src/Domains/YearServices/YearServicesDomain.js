"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.YearServicesDomain = void 0;
var YearServicesDomain = /** @class */ (function () {
    function YearServicesDomain(year, servicePrices) {
        this._year = year;
        this._servicePrices = servicePrices ? servicePrices : [];
    }
    Object.defineProperty(YearServicesDomain.prototype, "year", {
        get: function () { return this._year; },
        enumerable: false,
        configurable: true
    });
    YearServicesDomain.create = function (year, servicePrices) {
        return new YearServicesDomain(year, servicePrices);
    };
    YearServicesDomain.prototype.getServicePrice = function (serviceType) {
        var _a;
        return ((_a = this._servicePrices.find(function (item) { return item.type === serviceType; })) === null || _a === void 0 ? void 0 : _a.price) || 0;
    };
    return YearServicesDomain;
}());
exports.YearServicesDomain = YearServicesDomain;
