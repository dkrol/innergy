import { YearServicesDomain } from "./YearServicesDomain";
import { ServicePricesDomain } from "../ServicePrices/ServicePricesDomain";

describe("YearServicesDomain.getServicePrice", () => {
	test("should return zero if service not exist", () => {
		const yearServicesDomain = YearServicesDomain.create(2020, []);
		const result = yearServicesDomain.getServicePrice("Photography");
		expect(result).toEqual(0);
	});

	test("should return price if service exist", () => {
		const yearServicesDomain = YearServicesDomain.create(2020, [ServicePricesDomain.create("VideoRecording", 1700)]);
		const result = yearServicesDomain.getServicePrice("VideoRecording");
		expect(result).toEqual(1700);
	});
});