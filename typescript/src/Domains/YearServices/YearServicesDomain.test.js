"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var YearServicesDomain_1 = require("./YearServicesDomain");
var ServicePricesDomain_1 = require("../ServicePrices/ServicePricesDomain");
describe("YearServicesDomain.getServicePrice", function () {
    test("should return zero if service not exist", function () {
        var yearServicesDomain = YearServicesDomain_1.YearServicesDomain.create(2020, []);
        var result = yearServicesDomain.getServicePrice("Photography");
        expect(result).toEqual(0);
    });
    test("should return price if service exist", function () {
        var yearServicesDomain = YearServicesDomain_1.YearServicesDomain.create(2020, [ServicePricesDomain_1.ServicePricesDomain.create("VideoRecording", 1700)]);
        var result = yearServicesDomain.getServicePrice("VideoRecording");
        expect(result).toEqual(1700);
    });
});
