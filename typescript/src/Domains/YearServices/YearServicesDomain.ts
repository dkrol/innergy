import { ServiceYear, ServiceType } from "../../Models/Types";
import { ServicePricesDomain } from "../ServicePrices/ServicePricesDomain";

export class YearServicesDomain {
	private _year: ServiceYear;
	private _servicePrices: ServicePricesDomain[];

	public get year(): ServiceYear { return this._year; }

	private constructor(year: ServiceYear, servicePrices: ServicePricesDomain[]) {
		this._year = year;
		this._servicePrices = servicePrices ? servicePrices : [];
	}

	public static create(year: ServiceYear, servicePrices: ServicePricesDomain[]): YearServicesDomain {
		return new YearServicesDomain(year, servicePrices);
	}

	public getServicePrice(serviceType: ServiceType): number {
		return this._servicePrices.find(item => item.type === serviceType)?.price || 0;
	}
}