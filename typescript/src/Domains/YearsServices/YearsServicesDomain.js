"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.YearsServicesDomain = void 0;
var YearServicesDomain_1 = require("../YearServices/YearServicesDomain");
var ServicePricesDomain_1 = require("../ServicePrices/ServicePricesDomain");
var yearsServices = [
    YearServicesDomain_1.YearServicesDomain.create(2020, [
        ServicePricesDomain_1.ServicePricesDomain.create("Photography", 1700),
        ServicePricesDomain_1.ServicePricesDomain.create("VideoRecording", 1700),
        ServicePricesDomain_1.ServicePricesDomain.create("WeddingSession", 600),
        ServicePricesDomain_1.ServicePricesDomain.create("BlurayPackage", 300),
        ServicePricesDomain_1.ServicePricesDomain.create("TwoDayEvent", 400),
    ]),
    YearServicesDomain_1.YearServicesDomain.create(2021, [
        ServicePricesDomain_1.ServicePricesDomain.create("Photography", 1800),
        ServicePricesDomain_1.ServicePricesDomain.create("VideoRecording", 1800),
        ServicePricesDomain_1.ServicePricesDomain.create("WeddingSession", 600),
        ServicePricesDomain_1.ServicePricesDomain.create("BlurayPackage", 300),
        ServicePricesDomain_1.ServicePricesDomain.create("TwoDayEvent", 400),
    ]),
    YearServicesDomain_1.YearServicesDomain.create(2022, [
        ServicePricesDomain_1.ServicePricesDomain.create("Photography", 1900),
        ServicePricesDomain_1.ServicePricesDomain.create("VideoRecording", 1900),
        ServicePricesDomain_1.ServicePricesDomain.create("WeddingSession", 600),
        ServicePricesDomain_1.ServicePricesDomain.create("BlurayPackage", 300),
        ServicePricesDomain_1.ServicePricesDomain.create("TwoDayEvent", 400),
    ])
];
var YearsServicesDomain = /** @class */ (function () {
    function YearsServicesDomain() {
    }
    YearsServicesDomain.create = function () {
        return new YearsServicesDomain();
    };
    YearsServicesDomain.prototype.getYearServices = function (year) {
        var yearServices = yearsServices.find(function (item) { return item.year === year; });
        return yearServices ? yearServices : YearServicesDomain_1.YearServicesDomain.create(year, []);
    };
    return YearsServicesDomain;
}());
exports.YearsServicesDomain = YearsServicesDomain;
