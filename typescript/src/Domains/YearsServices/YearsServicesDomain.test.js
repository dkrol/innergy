"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var YearsServicesDomain_1 = require("./YearsServicesDomain");
describe.each([
    2020, 2021, 2022
])("YearsServicesDomain.getYearServices", function (year) {
    test("should return year services", function () {
        var yearsServicesDomain = YearsServicesDomain_1.YearsServicesDomain.create();
        var result = yearsServicesDomain.getYearServices(year);
        expect(result).not.toEqual(undefined);
        expect(result).not.toEqual(null);
    });
});
