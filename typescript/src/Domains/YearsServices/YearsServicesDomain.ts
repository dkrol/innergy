import { YearServicesDomain } from "../YearServices/YearServicesDomain";
import { ServicePricesDomain } from "../ServicePrices/ServicePricesDomain";
import { ServiceYear } from "../../Models/Types";

const yearsServices: YearServicesDomain[] = [
	YearServicesDomain.create(2020, [
		ServicePricesDomain.create("Photography", 1700),
		ServicePricesDomain.create("VideoRecording", 1700),
		ServicePricesDomain.create("WeddingSession", 600),
		ServicePricesDomain.create("BlurayPackage", 300),
		ServicePricesDomain.create("TwoDayEvent", 400),
	]),
	YearServicesDomain.create(2021, [
		ServicePricesDomain.create("Photography", 1800),
		ServicePricesDomain.create("VideoRecording", 1800),
		ServicePricesDomain.create("WeddingSession", 600),
		ServicePricesDomain.create("BlurayPackage", 300),
		ServicePricesDomain.create("TwoDayEvent", 400),
	]),
	YearServicesDomain.create(2022, [
		ServicePricesDomain.create("Photography", 1900),
		ServicePricesDomain.create("VideoRecording", 1900),
		ServicePricesDomain.create("WeddingSession", 600),
		ServicePricesDomain.create("BlurayPackage", 300),
		ServicePricesDomain.create("TwoDayEvent", 400),
	])
];

export class YearsServicesDomain {
	private constructor() { }

	public static create(): YearsServicesDomain {
		return new YearsServicesDomain();
	}

	public getYearServices(year: ServiceYear): YearServicesDomain {
		const yearServices = yearsServices.find(item => item.year === year);
		return yearServices ? yearServices : YearServicesDomain.create(year, []);
	}
}