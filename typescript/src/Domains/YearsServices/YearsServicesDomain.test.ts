import { YearsServicesDomain } from "./YearsServicesDomain";
import { ServiceYear } from "../../Models/Types";

describe.each([
	2020, 2021, 2022
])("YearsServicesDomain.getYearServices", (year: ServiceYear) => {
	test("should return year services", () => {
		const yearsServicesDomain = YearsServicesDomain.create();
		const result = yearsServicesDomain.getYearServices(year);
		expect(result).not.toEqual(undefined);
		expect(result).not.toEqual(null);
	});
});