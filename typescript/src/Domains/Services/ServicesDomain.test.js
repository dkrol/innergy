"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ServicesDomain_1 = require("./ServicesDomain");
var YearServicesDomain_1 = require("../YearServices/YearServicesDomain");
var ServicePricesDomain_1 = require("../ServicePrices/ServicePricesDomain");
describe("ServicesDomain.addService", function () {
    test("should add when not added", function () {
        var servicesDomain = ServicesDomain_1.ServicesDomain.create([]);
        servicesDomain.addService("BlurayPackage");
        expect(servicesDomain.getServices()).toEqual(["BlurayPackage"]);
    });
    test("should not add when added", function () {
        var servicesDomain = ServicesDomain_1.ServicesDomain.create(["BlurayPackage"]);
        servicesDomain.addService("BlurayPackage");
        expect(servicesDomain.getServices()).toEqual(["BlurayPackage"]);
    });
});
describe("ServicesDomain.removeService", function () {
    test("should remove if exist", function () {
        var servicesDomain = ServicesDomain_1.ServicesDomain.create(["BlurayPackage"]);
        servicesDomain.removeService("BlurayPackage");
        expect(servicesDomain.getServices()).toEqual([]);
    });
});
describe("ServicesDomain.contains", function () {
    test("should return true if exist", function () {
        var servicesDomain = ServicesDomain_1.ServicesDomain.create(["BlurayPackage"]);
        var result = servicesDomain.contains("BlurayPackage");
        expect(result).toEqual(true);
    });
    test("should return false if not exist", function () {
        var servicesDomain = ServicesDomain_1.ServicesDomain.create(["BlurayPackage"]);
        var result = servicesDomain.contains("Photography");
        expect(result).toEqual(false);
    });
});
describe("ServicesDomain.includes", function () {
    test("should return true if services includes all sent services", function () {
        var servicesDomain = ServicesDomain_1.ServicesDomain.create(["BlurayPackage", "Photography", "VideoRecording"]);
        var result = servicesDomain.includes(["VideoRecording", "Photography"]);
        expect(result).toEqual(true);
    });
    test("should return false if services not includes all sent services", function () {
        var servicesDomain = ServicesDomain_1.ServicesDomain.create(["BlurayPackage", "Photography", "VideoRecording"]);
        var result = servicesDomain.includes(["Photography", "WeddingSession"]);
        expect(result).toEqual(false);
    });
});
describe.each([
    [YearServicesDomain_1.YearServicesDomain.create(2020, [
            ServicePricesDomain_1.ServicePricesDomain.create("Photography", 1700),
            ServicePricesDomain_1.ServicePricesDomain.create("VideoRecording", 1700),
            ServicePricesDomain_1.ServicePricesDomain.create("WeddingSession", 600),
        ]), ServicesDomain_1.ServicesDomain.create(["Photography", "VideoRecording"]), 3400],
    [YearServicesDomain_1.YearServicesDomain.create(2021, [
            ServicePricesDomain_1.ServicePricesDomain.create("WeddingSession", 600),
            ServicePricesDomain_1.ServicePricesDomain.create("BlurayPackage", 300),
            ServicePricesDomain_1.ServicePricesDomain.create("TwoDayEvent", 400),
        ]), ServicesDomain_1.ServicesDomain.create(["BlurayPackage", "TwoDayEvent"]), 700]
])("ServicesDomain.calculatePrice (%s)", function (yearServicesDomain, servicesDomain, expectedPrice) {
    test("price matches expected", function () {
        var result = servicesDomain.calculatePrice(yearServicesDomain);
        expect(result).toBe(expectedPrice);
    });
});
