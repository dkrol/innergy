import { ServicesDomain } from "./ServicesDomain";
import { YearServicesDomain } from "../YearServices/YearServicesDomain";
import { ServicePricesDomain } from "../ServicePrices/ServicePricesDomain";

describe("ServicesDomain.addService", () => {
	test("should add when not added", () => {
		const servicesDomain = ServicesDomain.create([]);
		servicesDomain.addService("BlurayPackage");
		expect(servicesDomain.getServices()).toEqual(["BlurayPackage"]);
	});

	test("should not add when added", () => {
		const servicesDomain = ServicesDomain.create(["BlurayPackage"]);
		servicesDomain.addService("BlurayPackage");
		expect(servicesDomain.getServices()).toEqual(["BlurayPackage"]);
	});
});

describe("ServicesDomain.removeService", () => {
	test("should remove if exist", () => {
		const servicesDomain = ServicesDomain.create(["BlurayPackage"]);
		servicesDomain.removeService("BlurayPackage");
		expect(servicesDomain.getServices()).toEqual([]);
	});
});

describe("ServicesDomain.contains", () => {
	test("should return true if exist", () => {
		const servicesDomain = ServicesDomain.create(["BlurayPackage"]);
		const result = servicesDomain.contains("BlurayPackage");
		expect(result).toEqual(true);
	});

	test("should return false if not exist", () => {
		const servicesDomain = ServicesDomain.create(["BlurayPackage"]);
		const result = servicesDomain.contains("Photography");
		expect(result).toEqual(false);
	});
});

describe("ServicesDomain.includes", () => {
	test("should return true if services includes all sent services", () => {
		const servicesDomain = ServicesDomain.create(["BlurayPackage", "Photography", "VideoRecording"]);
		const result = servicesDomain.includes(["VideoRecording", "Photography"]);
		expect(result).toEqual(true);
	});

	test("should return false if services not includes all sent services", () => {
		const servicesDomain = ServicesDomain.create(["BlurayPackage", "Photography", "VideoRecording"]);
		const result = servicesDomain.includes(["Photography", "WeddingSession"]);
		expect(result).toEqual(false);
	});
});

describe.each([
	[YearServicesDomain.create(2020, [
		ServicePricesDomain.create("Photography", 1700),
		ServicePricesDomain.create("VideoRecording", 1700),
		ServicePricesDomain.create("WeddingSession", 600),
	]), ServicesDomain.create(["Photography", "VideoRecording"]), 3400],
	[YearServicesDomain.create(2021, [
		ServicePricesDomain.create("WeddingSession", 600),
		ServicePricesDomain.create("BlurayPackage", 300),
		ServicePricesDomain.create("TwoDayEvent", 400),
	]), ServicesDomain.create(["BlurayPackage", "TwoDayEvent"]), 700]
])("ServicesDomain.calculatePrice (%s)", (yearServicesDomain: YearServicesDomain, servicesDomain: ServicesDomain, expectedPrice) => {
	test("price matches expected", () => {
		const result = servicesDomain.calculatePrice(yearServicesDomain);
		expect(result).toBe(expectedPrice);
	});
});
