import { ServiceType } from "../../Models/Types";
import { YearServicesDomain } from "../YearServices/YearServicesDomain";

export class ServicesDomain {
	private _services: ServiceType[];

	private constructor(services: ServiceType[]) {
		this._services = services
			? services.filter((item, index) => services.indexOf(item) === index)
			: [];
	}

	public static create(services: ServiceType[]): ServicesDomain {
		return new ServicesDomain(services);
	}

	public getServices() {
		return [...this._services];
	}

	public addService(service: ServiceType): void {
		if (!this.contains(service)) {
			this._services.push(service);
		}
	}

	public removeService(service: ServiceType): void {
		const index = this._services.indexOf(service);
		if (index > -1) {
			this._services.splice(index, 1);
		}
	}

	public isEmpty(): boolean {
		return this._services.length === 0;
	}

	public contains(service: ServiceType): boolean {
		return this._services.find(item => item == service) ? true : false;
	}

	public includes(services: ServiceType[]): boolean {
		let result = this._services.filter(service => services.includes(service));
		return result.length === services.length;
	}

	public calculatePrice(yearServicesDomain: YearServicesDomain): number {
		let result = 0;
		this._services.forEach(service => result += yearServicesDomain.getServicePrice(service));
		return result;
	}
}