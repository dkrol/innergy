"use strict";
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ServicesDomain = void 0;
var ServicesDomain = /** @class */ (function () {
    function ServicesDomain(services) {
        this._services = services
            ? services.filter(function (item, index) { return services.indexOf(item) === index; })
            : [];
    }
    ServicesDomain.create = function (services) {
        return new ServicesDomain(services);
    };
    ServicesDomain.prototype.getServices = function () {
        return __spreadArrays(this._services);
    };
    ServicesDomain.prototype.addService = function (service) {
        if (!this.contains(service)) {
            this._services.push(service);
        }
    };
    ServicesDomain.prototype.removeService = function (service) {
        var index = this._services.indexOf(service);
        if (index > -1) {
            this._services.splice(index, 1);
        }
    };
    ServicesDomain.prototype.isEmpty = function () {
        return this._services.length === 0;
    };
    ServicesDomain.prototype.contains = function (service) {
        return this._services.find(function (item) { return item == service; }) ? true : false;
    };
    ServicesDomain.prototype.includes = function (services) {
        var result = this._services.filter(function (service) { return services.includes(service); });
        return result.length === services.length;
    };
    ServicesDomain.prototype.calculatePrice = function (yearServicesDomain) {
        var result = 0;
        this._services.forEach(function (service) { return result += yearServicesDomain.getServicePrice(service); });
        return result;
    };
    return ServicesDomain;
}());
exports.ServicesDomain = ServicesDomain;
