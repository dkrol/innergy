"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.calculatePrice = exports.updateSelectedServices = void 0;
var ServiceActionHandlersFactory_1 = require("./Domains/ServiceActions/Factories/ServiceActionHandlersFactory");
var ServicesDomain_1 = require("./Domains/Services/ServicesDomain");
var YearsServicesDomain_1 = require("./Domains/YearsServices/YearsServicesDomain");
var PriceCalculatorManager_1 = require("./Managers/PriceCalculatorManager");
exports.updateSelectedServices = function (previouslySelectedServices, action) {
    try {
        var handler = ServiceActionHandlersFactory_1.ServiceActionHandlersFactory.create().getItem(action.type);
        return handler.updateServices(action.service, ServicesDomain_1.ServicesDomain.create(previouslySelectedServices));
    }
    catch (e) {
        console.log(e);
    }
    return [];
};
exports.calculatePrice = function (selectedServices, selectedYear) {
    try {
        var services = ServicesDomain_1.ServicesDomain.create(selectedServices);
        var yearServices = YearsServicesDomain_1.YearsServicesDomain.create().getYearServices(selectedYear);
        var priceCalculatorManager = PriceCalculatorManager_1.PriceCalculatorManager.create(services, yearServices);
        return priceCalculatorManager.calculateBasePriceWithFinalPrice();
    }
    catch (e) {
        console.log(e);
    }
    return { basePrice: 0, finalPrice: 0 };
};
